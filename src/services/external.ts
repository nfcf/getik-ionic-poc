import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';
import { Device } from '@ionic-native/device';

@Injectable()
export class ExternalService {
  constructor(private device: Device,
              private inAppBrowser: InAppBrowser,
              private appAvailability: AppAvailability) {
  }

  openVascoApp() {
    this.launchExternalApp('digipass://', 'com.DIGIPASS', 'digipass://ro', 'https://play.google.com/store/apps/details?id=com.DIGIPASS');
  }

  private launchExternalApp(iosSchemaName: string, androidPackageName: string, appUrl: string, httpUrl: string) {
    let app: string;
    if (this.device.platform.toLowerCase() === 'ios') {
      app = iosSchemaName;
    } else if (this.device.platform.toLowerCase() === 'android') {
      app = androidPackageName;
    } else {
      let browser = this.inAppBrowser.create(httpUrl, '_system');
      return;
    }

    this.appAvailability.check(app).then(
      () => { // success callback
        let browser = this.inAppBrowser.create(appUrl, '_system');
      },
      () => { // error callback
        let browser = this.inAppBrowser.create(httpUrl, '_system');
      }
    );
  }

  

}