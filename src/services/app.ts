import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class AppService {
  private subject = new Subject<string>();

  constructor() {
  }

  signalOpenURL(url: string): void {
    this.subject.next(url);
  }

  handleOpenURL(): Observable<string> {
    return this.subject.asObservable();
  }

}