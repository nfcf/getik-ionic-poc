import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/index';
import { AppService } from '../services/index';


interface IWindow extends Window {
  handleOpenURL: Function;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private appService: AppService) {
    this.platform.ready().then(() => {
      (<IWindow>window).handleOpenURL = this.handleOpenURL.bind(this);

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  private handleOpenURL(url: string) {
    // alert('received url: ' + url);
    this.appService.signalOpenURL(url);
  }
}

