import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Inner1Page } from '../index';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goToOtherPage() {
    this.navCtrl.push(Inner1Page);
  }

}
