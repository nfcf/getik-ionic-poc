import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage, Inner3Page } from '../index';

@Component({
  selector: 'page-inner2',
  templateUrl: 'inner2.html'
})
export class Inner2Page {

  constructor(public navCtrl: NavController) {

  }

  goToOtherPage() {
    this.navCtrl.push(Inner3Page);
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }
}
