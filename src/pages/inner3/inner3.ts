import { Component, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { HomePage } from '../index';
import { AppService, ExternalService } from '../../services/index';

@Component({
  selector: 'page-inner3',
  templateUrl: 'inner3.html'
})
export class Inner3Page implements OnDestroy {
  readonly VASCO_CALLBACK_URL = 'mconect://BeforeLoginPage/MainMenuPage/TalkToVascoPage';
  subscription: Subscription;
  token: string;

  constructor(private navCtrl: NavController,
              private appService: AppService,
              private externalService: ExternalService) {
    this.subscription = this.appService.handleOpenURL().subscribe(
      (url: string) => {
        // Just making sure we got the right URL back...we could go straight to the 'toke' param
        if (url.indexOf(this.VASCO_CALLBACK_URL) >= 0) {
          this.token = this.getQueryParam(url, 'toke'); 
        } else {
          alert('Unexpected Vasco callback URL. Ignoring token...');
        }
      },
      (error: any) => {
        alert('Error:' + error);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openVascoApp() {
    this.externalService.openVascoApp();
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }

  private getQueryParam(url: string, param: string) {
    let query = url.substring(url.indexOf('?') + 1);
    let vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (pair[0] === param) {
        return pair[1];
      }
    }
    return undefined;
  }

}
