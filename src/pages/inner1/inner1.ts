import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage, Inner2Page } from '../index';

@Component({
  selector: 'page-inner1',
  templateUrl: 'inner1.html'
})
export class Inner1Page {

  constructor(public navCtrl: NavController) {

  }

  goToOtherPage() {
    this.navCtrl.push(Inner2Page);
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }

}
