# Getik Ionic PoC  #
    To get the project running on a development environment you'll first need to clone this repo.

### Pre-Requisites ###
    sudo npm install -g cordova ionic
    npm install @angular/cli

### Install dependencies ###
    cd getik-ionic-poc
    npm install
    cordova prepare

### Build project and serve it on http://localhost:8100/
    ionic serve

### Build and run the app directly on an emulator or on a connected Android device
    ionic cordova run android


To Test the Custom URL Scheme opener from ADB:

```bash
$ adb shell am start -a android.intent.action.VIEW -d "mconect://BeforeLoginPage/MainMenuPage/TalkToVascoPage?token=123"
```

